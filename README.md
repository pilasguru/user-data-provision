# user-data files for cloud instances provision

Repository with different user-data (bash scripts or cloud-init) to provision
new cloud instances at setup.


## References

* [cloud-init: the standard for customising cloud instances](https://cloud-init.io/)
* [cloud-init documentation](https://cloudinit.readthedocs.io/en/latest/index.html)
* [Run commands on your Linux instance at launch - AWS](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html)


## License

```
Copyright © 2022 Rodolfo Pilas <pilasguru@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the LICENSE file for more details.
```
